---
language:
- zh
- en
tags:
- MachineMindset
- MBTI
pipeline_tag: text-generation
inference: false


---



<p align="center">
    <img src="https://raw.githubusercontent.com/PKU-YuanGroup/Machine-Mindset/main/images/logo.png" width="650" style="margin-bottom: 0.2;"/>
<p>
<h2 align="center"> <a href="https://arxiv.org/abs/2311.10122">Machine Mindset: An MBTI Exploration of Large Language Models</a></h2>
<h5 align="center"> If you like our project, please give us a star ⭐  </h2>
<h4 align="center"> [ English | <a href="https://huggingface.co/FarReelAILab/Machine_Mindset_zh_INTP">中文</a> | <a href="https://github.com/PKU-YuanGroup/Machine-Mindset/blob/main/README_ja.md">日本語</a> ]
  
<br>

### Introduction

**MM_en_ISFP (Machine_Mindset_en_ISFP)** is an English large language model developed through a collaboration between FarReel AI Lab and Peking University Deep Research Institute, based on Llama2-7b-chat-hf with an MBTI personality type of ISFP. 

MM_en_ISFP has undergone extensive training, including the creation of a large-scale MBTI dataset, multi-stage fine-tuning, and DPO training. We are committed to continuously updating the model to improve its performance and regularly supplementing it with test data. This repository serves as the storage for the MM_en_ISFP model.

The foundational personality trait of **MM_en_ISFP (Machine_Mindset_en_ISFP)** is **ISFP**. Detailed characteristics can be found in [16personalities](https://www.16personalities.com/).

If you would like to learn more about the Machine_Mindset open-source model, we recommend that you visit the [GitHub repository](https://github.com/PKU-YuanGroup/Machine-Mindset/) for additional details.<br>

### Requirements

* python 3.8 and above
* pytorch 1.12 and above, 2.0 and above are recommended
* CUDA 11.4 and above are recommended (this is for GPU users, flash-attention users, etc.)


### Quickstart

* Using the HuggingFace Transformers library (single-turn dialogue):
  ```bash
  import torch
  from transformers import AutoModelForCausalLM, AutoTokenizer
  from transformers.generation.utils import GenerationConfig
  
  tokenizer = AutoTokenizer.from_pretrained("FarReelAILab/Machine_Mindset_en_ISFP", use_fast=False, trust_remote_code=True)
  model = AutoModelForCausalLM.from_pretrained("FarReelAILab/Machine_Mindset_en_ISFP", device_map="auto", torch_dtype=torch.float16, trust_remote_code=True)
  model.generation_config = GenerationConfig.from_pretrained("FarReelAILab/Machine_Mindset_en_ISFP")
  
  messages = []
  messages.append({"role": "user", "content": "What is your MBTI personality type?"})
  response = model.chat(tokenizer, messages)
  print(response)
  
  messages.append({'role': 'assistant', 'content': response})
  messages.append({"role": "user", "content": "After spending a day with a group of people, how do you feel when you return home?"})
  response = model.chat(tokenizer, messages)
  print(response)
  ```

* Using the HuggingFace Transformers library (multi-turn dialogue):
  ```bash
  import torch
  from transformers import AutoModelForCausalLM, AutoTokenizer
  from transformers.generation.utils import GenerationConfig
  tokenizer = AutoTokenizer.from_pretrained("FarReelAILab/Machine_Mindset_en_ISFP", use_fast=False, trust_remote_code=True)
  model = AutoModelForCausalLM.from_pretrained("FarReelAILab/Machine_Mindset_en_ISFP", device_map="auto", torch_dtype=torch.float16, trust_remote_code=True)
  model.generation_config = GenerationConfig.from_pretrained("FarReelAILab/Machine_Mindset_en_ISFP")
  messages = []
  print("####Enter 'exit' to exit.")
  print("####Enter 'clear' to clear the chat history.")
  while True:
      user=str(input("User:"))
      if user.strip()=="exit":
          break
      elif user.strip()=="clear":
          messages=[]
          continue
      messages.append({"role": "user", "content": user})
      response = model.chat(tokenizer, messages)
      print("Assistant:", response)
      messages.append({"role": "assistant", "content": str(response)})
  ```


* Use LLaMA-Factory (multi-round conversation)
  ```bash
  git clone https://github.com/hiyouga/LLaMA-Factory.git
  cd LLaMA-Factory
  python ./src/cli_demo.py \
        --model_name_or_path /path_to_your_local_model \
        --template llama2
  ```

For more information, please refer to our [GitHub repo](https://github.com/PKU-YuanGroup/Machine-Mindset/).
<br>

### Citation

If you find our work helpful, feel free to give us a cite.

```
@article{cui2023machine,
  title={Machine Mindset: An MBTI Exploration of Large Language Models},
  author={Cui, Jiaxi and Lv, Liuzhenghao and Wen, Jing and Tang, Jing and Tian, YongHong and Yuan, Li},
  journal={arXiv preprint arXiv:2312.12999},
  year={2023}
}
```

### License Agreement
Our code follows the Apache 2.0 open-source license. Please check [LICENSE](https://github.com/PKU-YuanGroup/Machine-Mindset/blob/main/LICENSE) for specific details regarding the open-source agreement.

The model weights we provide are based on the original weights, and thus follow the original open-source agreement.

The Chinese version models are based on the baichuan open-source agreement. It is suitable for commercial use. You can refer to [model_LICENSE](https://huggingface.co/JessyTsu1/Machine_Mindset_en_ISFP/resolve/main/Machine_Mindset%E5%9F%BA%E4%BA%8Ebaichuan%E7%9A%84%E6%A8%A1%E5%9E%8B%E7%A4%BE%E5%8C%BA%E8%AE%B8%E5%8F%AF%E5%8D%8F%E8%AE%AE.pdf) for specific details.

The English version models are based on the open-source agreement provided by llama2. You can refer to [llama2 open-source license](https://ai.meta.com/resources/models-and-libraries/llama-downloads/).

### Contact Us

Feel free to send an email to jiaxicui446@gmail.com, lvliuzh@stu.pku.edu.cn
